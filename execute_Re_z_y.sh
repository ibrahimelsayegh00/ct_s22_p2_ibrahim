echo "Running the input preparation for the turbulent simulation"
n=1
# for an integer n equal to 1 
for Re in 50000 100000 500000 1000000
do
	m=1
	for zMesh in 512 1024
	do
		o=1
		for yMesh in 128 256
		do
			mkdir Ibrahim${n}_${m}_${o}
			# will create a file called simulation and numbered with each value of n , m and 
			cd originalF
			# go to the directorie originalF
			sed -e "s/rrrrrrr/${Re}/" -e "s/xxxx/${zMesh}/" -e "s/yyy/${yMesh}/" inputOrig > input.dat
			# will save the file and the input of each  one 
			cp input.dat ../Ibrahim${n}_${m}_${o}
			# it will copy the file each time 
			cd ../
			# it will change directory 
			
			o=$(( $o + 1 ))
			# increment the integer by 1 
		done
		m=$(( $m + 1 ))
		# increment the integer by 1
	done
	n=$(( $n + 1 ))
	# increment the integer by 1
done
echo "input file preparation is done"
# the function is done
echo "for each value of n a folder will be created with a value of n combined with a number of the integer m and an integer o, after that the an increment of m will be added so the a folder will be at the same integer of n and o but different of m, the function will continue working till we use create folders with each possible value of these 3 integers"
